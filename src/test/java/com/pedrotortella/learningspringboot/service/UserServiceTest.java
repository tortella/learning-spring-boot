package com.pedrotortella.learningspringboot.service;

import com.pedrotortella.learningspringboot.dao.FakeDataDao;
import com.pedrotortella.learningspringboot.model.User;
import com.sun.javafx.collections.ImmutableObservableList;
import com.sun.org.apache.xpath.internal.Arg;
import jersey.repackaged.com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class UserServiceTest {
    private static final UUID TEST_UUID = UUID.randomUUID();
    private static final User TEST_USER = new User(TEST_UUID, "Rodolfo",
            "Oliveira", User.Gender.MALE, 25,"rodolfinho@gmail.com");
    private static final User INSERT_USER = new User(null, "Rodolfo",
            "Oliveira", User.Gender.MALE, 25,"rodolfinho@gmail.com");
    private static final UUID TEST_UUID2 = UUID.randomUUID();
    private static final User TEST_USER2 = new User(TEST_UUID2, "Rodolfinha",
            "Oliveira", User.Gender.FEMALE, 25,"rodolfinho@gmail.com");

    @Mock
    private FakeDataDao fakeDataDao;
    private UserService userService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        userService = new UserService(fakeDataDao);
    }

    @Test
    public void shouldGetAllUsers() {
        ImmutableList<User> users = new ImmutableList.Builder<User>()
                .add(TEST_USER)
                .build();

        given(fakeDataDao.selectAllUsers()).willReturn(users);
        List<User> allUsers = userService.getAllUsers(Optional.empty());

        assertThat(allUsers).hasSize(1);

        User user = allUsers.get(0);

        assertUserFields(user);
    }

    @Test
    public void shouldGetAllUsersByGender() {
        ImmutableList<User> users = new ImmutableList.Builder<User>()
                .add(TEST_USER)
                .add(TEST_USER2)
                .build();

        given(fakeDataDao.selectAllUsers()).willReturn(users);
        List<User> allUsers = userService.getAllUsers(Optional.of("MALE"));

        assertThat(allUsers).hasSize(1);

        User user = allUsers.get(0);

        assertUserFields(user);
    }

    @Test
    public void shouldGetUser() {
        given(fakeDataDao.selectUserByUserUid(TEST_UUID)).willReturn(Optional.of(TEST_USER));

        Optional<User> rodolfoOptional = userService.getUser(TEST_UUID);

        assertThat(rodolfoOptional.isPresent()).isTrue();
        User rodolfo = rodolfoOptional.get();
        assertUserFields(rodolfo);
    }

    @Test
    public void shouldUpdateUser() {
        given(fakeDataDao.selectUserByUserUid(TEST_UUID)).willReturn(Optional.of(TEST_USER));
        given(fakeDataDao.updateUser(TEST_USER)).willReturn(1);

        ArgumentCaptor<User> captor = ArgumentCaptor.forClass(User.class);
        int updateResult = userService.updateUser(TEST_USER);

        verify(fakeDataDao).selectUserByUserUid(TEST_UUID);
        verify(fakeDataDao).updateUser(captor.capture());

        User user = captor.getValue();
        assertUserFields(user);
        assertThat(updateResult).isEqualTo(1);
    }

    @Test
    public void shouldRemoveUser() {
        given(fakeDataDao.selectUserByUserUid(TEST_UUID)).willReturn(Optional.of(TEST_USER));
        given(fakeDataDao.deleteUserByUserUid(TEST_UUID)).willReturn(1);

        int deleteResult = userService.removeUser(TEST_UUID);

        verify(fakeDataDao).selectUserByUserUid(TEST_UUID);
        verify(fakeDataDao).deleteUserByUserUid(TEST_UUID);
        assertThat(deleteResult).isEqualTo(1);
    }

    @Test
    public void insertUser() {
        given(fakeDataDao.insertUser(any(UUID.class), eq(INSERT_USER))).willReturn(1);

        ArgumentCaptor<User> captor = ArgumentCaptor.forClass(User.class);
        int insertResult = userService.insertUser(INSERT_USER);

        verify(fakeDataDao).insertUser(any(UUID.class), captor.capture());

        User user = captor.getValue();
        assertUserFields(user);
        assertThat(insertResult).isEqualTo(1);
    }

    private void assertUserFields(User user) {
        assertThat(user.getAge()).isEqualTo(25);
        assertThat(user.getFirstName()).isEqualTo("Rodolfo");
        assertThat(user.getLastName()).isEqualTo("Oliveira");
        assertThat(user.getGender()).isEqualTo(User.Gender.MALE);
        assertThat(user.getEmail()).isEqualTo("rodolfinho@gmail.com");
        assertThat(user.getUserUid()).isNotNull();
    }
}