package com.pedrotortella.learningspringboot.dao;

import com.pedrotortella.learningspringboot.model.User;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

public class FakeDataDaoTest {

    private FakeDataDao fakeDataDao;

    @Before
    public void setUp() throws Exception {
        fakeDataDao = new FakeDataDao();
    }

    @Test
    public void shouldSelectAllUsers() {
        List<User> users = fakeDataDao.selectAllUsers();
        assertThat(users).hasSize(1);

        User user = users.get(0);

        assertThat(user.getAge()).isEqualTo(24);
        assertThat(user.getFirstName()).isEqualTo("Pedro");
        assertThat(user.getLastName()).isEqualTo("Tortella");
        assertThat(user.getGender()).isEqualTo(User.Gender.MALE);
        assertThat(user.getEmail()).isEqualTo("pedro.tortella@gmail.com");
        assertThat(user.getUserUid()).isNotNull();
    }

    @Test
    public void shouldSelectUserByUserUid() {
        UUID rodolfoUserUid = UUID.randomUUID();
        User rodolfo = new User(rodolfoUserUid, "Rodolfo", "Oliveira", User.Gender.MALE, 25,
                "rodolfinho@gmail.com");
        fakeDataDao.insertUser(rodolfoUserUid, rodolfo);

        assertThat(fakeDataDao.selectAllUsers()).hasSize(2);

        Optional<User> rodolfoOptional = fakeDataDao.selectUserByUserUid(rodolfoUserUid);
        assertThat(rodolfoOptional.isPresent()).isTrue();
        assertThat(rodolfoOptional.get()).isEqualToComparingFieldByField(rodolfo);
    }

    @Test
    public void shouldNotSelectUserByRandomUserUid() {
        Optional<User> user = fakeDataDao.selectUserByUserUid(UUID.randomUUID());
        assertThat(user.isPresent()).isFalse();
    }

    @Test
    public void shouldUpdateUser() {
        UUID joeUserUid = fakeDataDao.selectAllUsers().get(0).getUserUid();
        User updatedJoe = new User(joeUserUid, "Rodolfo", "Oliveira", User.Gender.MALE, 25,
                "rodolfinho@gmail.com");
        fakeDataDao.updateUser(updatedJoe);

        assertThat(fakeDataDao.selectAllUsers()).hasSize(1);

        Optional<User> joeOptional = fakeDataDao.selectUserByUserUid(joeUserUid);
        assertThat(joeOptional.isPresent()).isTrue();
        assertThat(joeOptional.get()).isEqualToComparingFieldByField(updatedJoe);
    }

    @Test
    public void shouldDeleteUserByUserUid() {
        UUID joeUserUid = fakeDataDao.selectAllUsers().get(0).getUserUid();
        fakeDataDao.deleteUserByUserUid(joeUserUid);

        assertThat(fakeDataDao.selectUserByUserUid(joeUserUid).isPresent()).isFalse();
        assertThat(fakeDataDao.selectAllUsers()).isEmpty();
    }

    @Test
    public void shouldInsertUser() {
        UUID rodolfoUserUid = UUID.randomUUID();
        User rodolfo = new User(rodolfoUserUid, "Rodolfo", "Oliveira", User.Gender.MALE, 25,
                "rodolfinho@gmail.com");
        fakeDataDao.insertUser(rodolfoUserUid, rodolfo);

        assertThat(fakeDataDao.selectAllUsers()).hasSize(2);

        Optional<User> rodolfoOptional = fakeDataDao.selectUserByUserUid(rodolfoUserUid);
        assertThat(rodolfoOptional.isPresent()).isTrue();
        assertThat(rodolfoOptional.get()).isEqualToComparingFieldByField(rodolfo);
    }
}